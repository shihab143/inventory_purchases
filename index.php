<?php
session_start();
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Welcome to our inventory Management</title>
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

</head>
<body style="background-image: url('https://armsit.com/wp-content/uploads/2022/02/woocommerce-inventory-management.png');background-size: cover; background-repeat: no-repeat;">
<div class="container text-white">
    <div class="row">
        <div class="col-md-4 offset-md-4 col-sm-12 mt-5">
            <?php if (isset($_SESSION['success'])) { ?>
                <div class="mt-5 mb-5">
                    <span class="alert alert-success d-block"><?php echo $_SESSION['success'] ?></span>
                </div>
                <?php unset($_SESSION['success']);
            } ?>
            <div class="card border-primary mb-3" style="background: transparent;">
                <div class="card-header ">Login to Inventory Management</div>
                <div class="card-body ">
                    <form method="post" action="login.php">
                        <div class="mb-3">
                            <label for="phone" class="form-label">Phone</label>
                            <input type="text" class="form-control" name="phone" id="phone" placeholder="Enter Phone">
                        </div>
                        <div class="mb-3">
                            <label for="password" class="form-label">Password</label>
                            <input type="password" class="form-control" name="password" id="password" placeholder="Enter Password">
                        </div>
                        <?php if (isset($_SESSION['error'])) { ?>
                            <div class="mt-5 mb-5">
                                <span class="alert alert-danger d-block"><?php echo $_SESSION['error'] ?></span>
                            </div>
                            <?php unset($_SESSION['error']);
                        } ?>
                        <button type="submit" class="btn btn-success float-end">Login</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"></script>
</body>
</html>