-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 05, 2022 at 06:41 PM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 7.4.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inventory_purchase`
--

-- --------------------------------------------------------

--
-- Table structure for table `purchases`
--

CREATE TABLE `purchases` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `purchase_date` date NOT NULL,
  `purchase_info` varchar(255) NOT NULL,
  `amount` double(8,2) NOT NULL,
  `paid` double(8,2) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `purchases`
--

INSERT INTO `purchases` (`id`, `customer_id`, `purchase_date`, `purchase_info`, `amount`, `paid`, `created_at`) VALUES
(1, 2, '2022-03-25', 'Oil', 500.00, 300.00, '2022-03-25 22:54:03'),
(2, 2, '2022-03-25', 'Dal', 230.00, 100.00, '2022-03-25 22:54:43'),
(3, 8, '2022-03-16', 'coffee ', 600.00, 200.00, '2022-03-25 23:44:09'),
(4, 4, '2022-03-19', 'Computer', 4000.00, 500.00, '2022-03-25 23:44:33'),
(7, 8, '2022-04-05', 'coffee ', 1000.00, 300.00, '2022-04-05 19:54:29');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `age` int(5) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `type` char(1) NOT NULL DEFAULT '0' COMMENT '1=admin,0=customer',
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `phone`, `password`, `age`, `address`, `type`, `created_at`) VALUES
(1, 'Admin', '01780597143', '202cb962ac59075b964b07152d234b70', 16, 'Dhaka', '1', '2022-03-17 16:35:10'),
(2, 'Rajon', '01256', NULL, 23, 'Mohamadpur', '0', '2022-03-25 14:20:34'),
(3, 'Nayem', '01735', NULL, 22, 'Jessore', '0', '2022-03-25 14:20:34'),
(4, 'Lima', '01254', NULL, 21, 'Dhaka,Bangladesh', '0', '2022-03-25 20:25:59'),
(5, 'Rumana', '03154', NULL, 24, 'Mirpur', '0', '2022-03-25 03:03:20'),
(6, 'Shakil', '03157', NULL, 23, 'adasd', '0', '2022-03-25 20:35:45'),
(7, 'Ramasis', '01845', NULL, 26, 'Bosundhara', '0', '2022-03-25 21:04:46'),
(8, 'Mazharul ', '01274', NULL, 25, 'Dhaka', '0', '2022-03-25 21:17:17'),
(9, 'Saif', '01685', NULL, 22, 'Dhaka', '0', '2022-03-25 21:17:32'),
(10, 'Osman ', '01683', NULL, 14, 'Bosundhara', '0', '2022-03-25 21:17:58'),
(11, 'Ritu ', '01647', NULL, 22, 'Sadarghat', '0', '2022-03-25 21:18:35'),
(12, 'Tusher ', '01945', NULL, 23, 'Noakhali', '0', '2022-03-25 21:21:17');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `purchases`
--
ALTER TABLE `purchases`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `purchases`
--
ALTER TABLE `purchases`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
