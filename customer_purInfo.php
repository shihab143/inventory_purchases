<?php
session_start();
error_reporting(0);
include_once 'connection.php';

if(empty($_SESSION["login_user"])){  
    $_SESSION['success'] = "Please login first to start your session";
    header("location: index.php");
} 
$error = '';
$customer_id = $_GET['id'];
$check_sql = "SELECT * from users where type = 0 and id = $customer_id";
$customer_query = mysqli_query($conn, $check_sql);
$customer = mysqli_fetch_assoc($customer_query);

// get purchases history by $customer_id
$history_query = "SELECT * from purchases where customer_id = $customer_id order by purchase_date ASC";
$get_cquery = mysqli_query($conn, $history_query);
$purchase_history = mysqli_fetch_all($get_cquery, MYSQLI_ASSOC);
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Customer Purchases History</title>
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>
<header class="p-3 bg-info text-white">
    <div class="container">
        <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
            <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
                <li><a href="dashboard.php" class="nav-link px-2 text-white">Home</a></li>
                <li><a href="customer/add.php" class="nav-link px-2 text-white">Customer Add</a></li>
                <li><a href="customer/list.php" class="nav-link px-2 text-secondary">Customers List</a></li>
                <li><a href="purchase/add.php" class="nav-link px-2 text-white">Purchase Add</a></li>
                <li><a href="purchase/list.php" class="nav-link px-2 text-white">Purchases List</a></li>
            </ul>
            <div class="text-end">
                <a href="logout.php" class="btn btn-outline-light me-2">Logout</a>
            </div>
        </div>
    </div>
</header>

<div class="container mt-5">
    <div class="row">
        <div class="col-sm-12">
            <h3>Customer Purchases History</h3>
            <a href="customer/list.php" class="btn btn-success  float-end">Back</a>
            <div class="">
                <table>
                    <body>
                    <tr>
                        <td>Customer Name</td>
                        <td>:</td>
                        <td><?php echo $customer['name'] ?></td>
                    </tr>
                    <tr>
                        <td>Customer Phone</td>
                        <td>:</td>
                        <td><?php echo $customer['phone'] ?></td>
                    </tr>
                    <tr>
                        <td>Customer Age</td>
                        <td>:</td>
                        <td><?php echo $customer['age'] ?></td>
                    </tr>
                    <tr>
                        <td>Customer Address</td>
                        <td>:</td>
                        <td><?php echo $customer['address'] ?></td>
                    </tr>
                    </body>
                </table>
            </div>
            <div class="">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Si</th>
                        <th>Purchase Date</th>
                        <th>Purchase Info</th>
                        <th>Amount</th>
                        <th>Paid</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i = 1;$tamount=$tpaid=0;
                    foreach ($purchase_history as $purchase) { ?>
                        <tr>
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $purchase['purchase_date'] ?></td>
                            <td><?php echo $purchase['purchase_info'] ?></td>
                            <td><?php echo $purchase['amount'];$tamount += $purchase['amount']; ?></td>
                            <td><?php echo $purchase['paid']; $tpaid += $purchase['paid']; ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="3" style="text-align: end">Total</td>
                        <td><?php echo number_format($tamount,2)?></td>
                        <td><?php echo number_format($tpaid,2)?></td>
                    </tr>
                    <tr>
                        <td colspan="3" style="text-align: end">Current Due</td>
                        <td colspan="2"><?php echo number_format($tamount-$tpaid,2)?></td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"></script>
</body>
</html>