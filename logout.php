<?php 

session_start();
error_reporting(0);
session_destroy();
session_unset();
unset($_SESSION["login_user"]);
$_SESSION['success'] = "Logout Successful";
header("location: index.php");
?>