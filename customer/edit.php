<?php
session_start();
error_reporting(0);
include_once '../connection.php';

if(empty($_SESSION["login_user"])){  
    $_SESSION['success'] = "Please login first to start your session";
    header("location: ../index.php");
} 


$error = '';
$customer_id = $_GET['id'];
$check_sql = "SELECT * from users where type = 0 and id = $customer_id";
$customer_query = mysqli_query($conn, $check_sql);
$customer = mysqli_fetch_assoc($customer_query);
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $name = $_POST['name'];
    $phone = $_POST['phone'];
    $age = $_POST['age'];
    $address = $_POST['address'];
    if (empty($name)) {
        $error = "The Name is required";
    } elseif (empty($phone)) {
        $error = "The Phone is required";
    } elseif (empty($age)) {
        $error = "The Age is required";
    } elseif (empty($address)) {
        $error = "The Address is required";
    } else {
        $check_sql = "SELECT * from users where type = 0 and id != $customer_id and phone = $phone";
        $exist = mysqli_query($conn, $check_sql);
        $exist_cus = mysqli_fetch_assoc($exist);
        if (isset($exist_cus['id'])) {
            $error = "The Customer Phone duplicate to another Customer";
        } else {
            $sql = "UPDATE users set name = '$name' ,phone = '$phone',age = $age,address = '$address' where id = $customer_id";
            if (mysqli_query($conn, $sql)) {
                $success = "Customer Update Successfully";
            } else {
                $error = "Error: " . $sql . "<br>" . mysqli_error($conn);
            }
        }
    }
}
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Customer Edit</title>
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>
<header class="p-3 bg-info text-white">
    <div class="container">
        <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
            <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
                <li><a href="../dashboard.php" class="nav-link px-2 text-white">Home</a></li>
                <li><a href="add.php" class="nav-link px-2 text-white">Customer Add</a></li>
                <li><a href="list.php" class="nav-link px-2 text-secondary">Customers List</a></li>
                <li><a href="../purchase/add.php" class="nav-link px-2 text-white">Purchase Add</a></li>
                <li><a href="../purchase/list.php" class="nav-link px-2 text-white">Purchases List</a></li>
            </ul>
            <div class="text-end">
                <a href="../logout.php" class="btn btn-outline-light me-2">Logout</a>
            </div>
        </div>
    </div>
</header>

<div class="container d-flex align-items-center justify-content-center mt-5">
    <div class="row">
        <div class="card" style="width: 400px;">
            <div class="card-header pl-0">Update Customer</div>
            <div class="card-body">
                <?php if (isset($success)) { ?>
                    <div class="mt-2 mb-2">
                        <span class="alert alert-success d-block"><?php echo $success ?></span>
                    </div>
                <?php } ?>
                <form method="post" action="edit.php?id=<?php echo $customer_id; ?>">
                    <div class="form-group">
                        <label for="name"><b>Name</b><code>*</code></label>
                        <input type="text" name="name" value="<?php echo $customer['name'] ?>" class="form-control"
                               id="name" placeholder="Enter Name"
                               style="background-color: #eff1f1;">
                    </div>
                    <div class="form-group">
                        <label for="phone" class="form-label mb-0"><b>Phone</b><code>*</code></label>
                        <input type="tel" class="form-control mt-0" id="phone" name="phone"
                               value="<?php echo $customer['phone'] ?>"
                               placeholder="Enter Mobile Number" style="background-color: #eff1f1;">
                    </div>
                    <div class="form-group">
                        <label for="age"><b>Age</b><code>*</code></label>
                        <input type="number" name="age" class="form-control" id="age" placeholder="Enter Age"
                               style="background-color: #eff1f1;" value="<?php echo $customer['age'] ?>">
                    </div>
                    <div class="form-group">
                        <label for="address"><b>Address</b><code>*</code></label>
                        <input type="text" name="address" class="form-control" placeholder="Enter address"
                               style="background-color: #eff1f1;" value="<?php echo $customer['address'] ?>">
                    </div>
                    <?php if ($error != '') { ?>
                        <div class="mt-3 mb-1">
                            <span class="alert alert-danger d-block"><?php echo $error ?></span>
                        </div>
                    <?php } ?>
                    <div class="form-group mt-3">
                        <button type="submit" name="register" class="registerbtn btn btn-warning btn-block">Update
                            Customer
                        </button>
                        <a href="list.php" class="btn btn-success  float-end">Back</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"></script>
</body>
</html>