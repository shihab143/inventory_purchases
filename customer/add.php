<?php
session_start();
error_reporting(0);
include_once '../connection.php';

if(empty($_SESSION["login_user"])){  
    $_SESSION['success'] = "Please login first to start your session";
    header("location: ../index.php");
} 
$error = '';
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $name = $_POST['name'];
    $phone = $_POST['phone'];
    $age = $_POST['age'];
    $address = $_POST['address'];
    if (empty($name)) {
        $error = "The Name is required";
    } elseif (empty($phone)) {
        $error = "The Phone is required";
    } elseif (empty($age)) {
        $error = "The Age is required";
    } elseif (empty($address)) {
        $error = "The Address is required";
    } else {
        $check_sql = "SELECT * from users where type = 0 and phone = $phone";
        $exist = mysqli_query($conn, $check_sql);
        $exist_cus = mysqli_fetch_assoc($exist);
        if (isset($exist_cus['id'])) {
            $error = "The Customer Phone Already exists";
        } else {
            $date = date('Y-m-d H:i:s');
            $sql = "INSERT INTO users (name,phone,age,address,created_at) VALUES ('$name','$phone','$age','$address','$date')";
            if (mysqli_query($conn, $sql)) {
                $success = "Customer Added Successfully";
            } else {
                $error = "Error: " . $sql . "<br>" . mysqli_error($conn);
            }
        }
    }
}
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Customer Add</title>
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>
<header class="p-3 bg-info text-white">
    <div class="container">
        <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
            <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
                <li><a href="../dashboard.php" class="nav-link px-2 text-white">Home</a></li>
                <li><a href="add.php" class="nav-link px-2 text-secondary">Customer Add</a></li>
                <li><a href="list.php" class="nav-link px-2 text-white">Customers List</a></li>
                <li><a href="../purchase/add.php" class="nav-link px-2 text-white">Purchase Add</a></li>
                <li><a href="../purchase/list.php" class="nav-link px-2 text-white">Purchases List</a></li>
            </ul>
            <div class="text-end">
                <a href="../logout.php" class="btn btn-outline-light me-2">Logout</a>
            </div>
        </div>
    </div>
</header>

<div class="container d-flex align-items-center justify-content-center mt-5">
    <div class="row">
        <div class="card" style="width: 400px;">
            <div class="card-header pl-0">Add Customer</div>
            <div class="card-body">
                <?php if (isset($success)) { ?>
                    <div class="mt-2 mb-2">
                        <span class="alert alert-success d-block"><?php echo $success ?></span>
                    </div>
                <?php } ?>
                <p>Please fill in this form to Add Customer</p>
                <code class="float-end">* field are required </code>
                <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                    <div class="form-group">
                        <label for="name"><b>Name</b><code>*</code></label>
                        <input type="text" name="name" value="" class="form-control" id="name" placeholder="Enter Name"
                               style="background-color: #eff1f1;">
                    </div>
                    <div class="form-group">
                        <label for="phone" class="form-label mb-0"><b>Phone No</b><code>*</code></label>
                        <input type="tel" class="form-control mt-0" id="phone" name="phone"
                               placeholder="Enter Mobile Number" style="background-color: #eff1f1;">
                    </div>
                    <div class="form-group">
                        <label for="age"><b>Age</b><code>*</code></label>
                        <input type="number" name="age" class="form-control" id="age" placeholder="Enter Age"
                               style="background-color: #eff1f1;">
                    </div>
                    <div class="form-group">
                        <label for="address"><b>Address</b><code>*</code></label>
                        <input type="text" name="address" class="form-control" placeholder="Enter address"
                               style="background-color: #eff1f1;">
                    </div>
                    <?php if ($error != '') { ?>
                        <div class="mt-3 mb-1">
                            <span class="alert alert-danger d-block"><?php echo $error ?></span>
                        </div>
                    <?php } ?>
                    <div class="form-group mt-3">
                        <button type="submit" name="register" class="registerbtn btn btn-warning btn-block">Add
                            Customer
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"></script>
</body>
</html>