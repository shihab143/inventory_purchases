<?php
session_start();
error_reporting(0);
include_once '../connection.php';

if(empty($_SESSION["login_user"])){  
    $_SESSION['success'] = "Please login first to start your session";
    header("location: ../index.php");
} 

$sql = "SELECT * from users where type = 0"; // type = 0 means customers
$result = mysqli_query($conn, $sql);
$row = mysqli_fetch_all($result, MYSQLI_ASSOC);

$age_to = $age_from = $address = $phone = $name = '';
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $name = $_POST['name'];
    $phone = $_POST['phone'];
    $address = $_POST['address'];
    $age_from = $_POST['age_from'];
    $age_to = $_POST['age_to'];

    $condition = 'type = 0';
    if (!empty($name)) {
        $condition .= " AND name like '%$name%'";
    }
    if (!empty($phone)) {
        $condition .= " AND phone = $phone";
    }
    if (!empty($address)) {
        $condition .= " AND address like '%$address%'";
    }
    if (!empty($age_from) && !empty($age_to)) {
        $condition .= " AND age between $age_from and $age_to";
    }
    $query = "SELECT * FROM users WHERE $condition";
    $query_execute = mysqli_query($conn, $query);
    $row = mysqli_fetch_all($query_execute, MYSQLI_ASSOC);
}
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Customer List</title>
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://use.fontawesome.com/51f0941df6.js"></script>
</head>
<body>
<header class="p-3 bg-info text-white">
    <div class="container">
        <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
            <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
                <li><a href="../dashboard.php" class="nav-link px-2 text-white">Home</a></li>
                <li><a href="add.php" class="nav-link px-2 text-white">Customer Add</a></li>
                <li><a href="list.php" class="nav-link px-2 text-secondary">Customers List</a></li>
                <li><a href="../purchase/add.php" class="nav-link px-2 text-white">Purchase Add</a></li>
                <li><a href="../purchase/list.php" class="nav-link px-2 text-white">Purchases List</a></li>
            </ul>
            <div class="text-end">
                <a href="../logout.php" class="btn btn-outline-light me-2">Logout</a>
            </div>
        </div>
    </div>
</header>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <?php if (isset($_SESSION['success'])) { ?>
                <div class="mt-2 mb-2">
                    <span class="alert alert-success text-center d-block"><?php echo $_SESSION['success'] ?></span>
                </div>
                <?php unset($_SESSION['success']);
            } ?>
            <?php if (isset($_SESSION['error'])) { ?>
                <div class="mt-2 mb-2">
                    <span class="alert alert-danger text-center  d-block"><?php echo $_SESSION['error'] ?></span>
                </div>
                <?php unset($_SESSION['error']);
            } ?>
            <h3>Customer List</h3>
            <div class="card">
                <div class="card-body">
                    <form action="list.php" method="post">
                        <div class="col-md-3 float-start">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" name="name" value="<?php echo $name ?>" class="form-control" id="name">
                            </div>
                        </div>
                        <div class="col-md-3 float-start">
                            <div class="form-group">
                                <label for="phone">Phone</label>
                                <input type="text" name="phone" value="<?php echo $phone ?>" class="form-control" id="phone">
                            </div>
                        </div>
                        <div class="col-md-3 float-start">
                            <div class="form-group">
                                <label for="address">Address</label>
                                <input type="text" name="address" value="<?php echo $address ?>" class="form-control" id="address">
                            </div>
                        </div>
                        <div class="col-md-3 float-start">
                            <div class="form-group" style="width: 40%;float: left;padding-right: 15px">
                                <label for="age_from">Age From</label>
                                <input type="number" name="age_from" min="0" value="<?php echo $age_from ?>" class="form-control" id="age_from">
                            </div>
                            <div class="form-group" style="width: 40%;float: left;padding-left: 15px">
                                <label for="age_to">Age To</label>
                                <input type="number" name="age_to" min="0" value="<?php echo $age_to ?>" class="form-control" id="age_to">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="mt-3 col-md-3">
                            <div class="form-group float-start" style="padding-right: 10px">
                                <button type="submit" class="btn btn-warning btn-block">
                                    Search
                                </button>
                            </div>
                            <div class="form-group">
                                <a href="list.php" class="btn btn-success btn-block">
                                    Clear
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="table-responsive">
            <table class="table table-bordered mt-3">
                <thead>
                <tr>
                    <th>SI</th>
                    <th>Customer Name</th>
                    <th>Customer Phone</th>
                    <th>Customer Age</th>
                    <th>Customer Address</th>
                    <th>Added Date</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php for ($i = 0; $i < count($row); $i++) { ?>
                    <tr>
                        <td><?php echo $i + 1 ?></td>
                        <td>
                            <a href="../customer_purInfo.php?id=<?php echo $row[$i]['id'] ?>"><?php echo $row[$i]['name'] ?></a>
                        </td>
                        <td><?php echo $row[$i]['phone'] ?></td>
                        <td><?php echo $row[$i]['age'] ?></td>
                        <td><?php echo $row[$i]['address'] ?></td>
                        <td><?php echo $row[$i]['created_at'] ?></td>
                        <td>
                            <a title="Edit Customer <?php echo $row[$i]['name'] ?>"
                               href="edit.php?id=<?php echo $row[$i]['id'] ?>"><i class="fa fa-pencil-square-o"></i></a>
                            <a title="Delete Customer <?php echo $row[$i]['name'] ?>"
                               href="delete.php?id=<?php echo $row[$i]['id'] ?>" class="text-danger"><i
                                        class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"></script>
<style>
    .col-md-3, .col-md-1 {
        padding-left: 15px;
        padding-right: 15px;
    }
    @media screen and (max-width: 600px){
        .wd-100{
            width:100% !important;
        }
        .float-start{float:none !important;}
    }
</style>

</body>
</html>