<?php
session_start();
include_once '../connection.php';
error_reporting(0);
$error = '';

if(empty($_SESSION["login_user"])){  
    $_SESSION['success'] = "Please login first to start your session";
    header("location: ../index.php");
} 

$purchase_id = $_GET['id'];
$check_sql = "SELECT * from purchases where id = $purchase_id";
$purchase_query = mysqli_query($conn, $check_sql);
$purchase = mysqli_fetch_assoc($purchase_query);

$customer_q = "SELECT * from users where type = 0 order by name";
$customer_query = mysqli_query($conn, $customer_q);
$customers = mysqli_fetch_all($customer_query,MYSQLI_ASSOC);

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $customer_id = $_POST['customer_id'];
    $purchase_date = $_POST['purchase_date'];
    $amount = $_POST['amount'];
    $paid = $_POST['paid'];
    $purchase_info = $_POST['purchase_info'];
    if ($customer_id == '') {
        $error = "Select an Customer";
    } elseif ($purchase_date == '') {
        $error = "The Purchase Date is required";
    } elseif ($amount == '') {
        $error = "The Amount is required";
    } elseif ($paid == '') {
        $error = "The Paid Amount is required";
    } elseif ($purchase_info == '') {
        $error = "The Purchase Info is required";
    } else {
        $sql = "Update purchases set customer_id = $customer_id,purchase_date = '$purchase_date',purchase_info = '$purchase_info',amount =$amount,paid = $paid where id = $purchase_id";
        if (mysqli_query($conn, $sql)) {
            $success = "Purchase Updated Successfully";
        } else {
            $error = "Error: " . $sql . "<br>" . mysqli_error($conn);
        }
    }
}
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Purchase Update</title>
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>
<header class="p-3 bg-info text-white">
    <div class="container">
        <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
            <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
                <li><a href="../dashboard.php" class="nav-link px-2 text-white">Home</a></li>
                <li><a href="../customer/add.php" class="nav-link px-2 text-white">Customer Add</a></li>
                <li><a href="../customer/list.php" class="nav-link px-2 text-white">Customers List</a></li>
                <li><a href="add.php" class="nav-link px-2 text-white">Purchase Add</a></li>
                <li><a href="list.php" class="nav-link px-2 text-secondary">Purchases List</a></li>
            </ul>
            <div class="text-end">
                <a href="../logout.php" class="btn btn-outline-light me-2">Logout</a>
            </div>
        </div>
    </div>
</header>

<div class="container d-flex align-items-center justify-content-center mt-5">
    <div class="row">
        <div class="card" style="width: 400px;">
            <div class="card-header pl-0">Update Purchase</div>
            <div class="card-body">
                <?php if (isset($success)) { ?>
                    <div class="mt-2 mb-2">
                        <span class="alert alert-success d-block"><?php echo $success ?></span>
                    </div>
                <?php } ?>
                <form method="post" action="edit.php?id=<?php echo $purchase_id ?>">
                    <div class="form-group">
                        <label for="customer_id">Customer</label>
                        <select name="customer_id" id="customer_id" class="form-control">
                            <option value="">Choose</option>
                            <?php for ($i=0;$i<count($customers);$i++){ ?>
                                <option value="<?php echo $customers[$i]['id']?>" <?php echo ($purchase['customer_id'] == $customers[$i]['id'] ? 'selected' : '') ?> ><?php echo $customers[$i]['name']?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="purchase_date">Purchase Date</label>
                        <input type="date" name="purchase_date" value="<?php echo $purchase['purchase_date']?>" class="form-control" id="purchase_date">
                    </div>
                    <div class="form-group">
                        <label for="amount">Amount</label>
                        <input type="number" min="0" name="amount" value="<?php echo $purchase['amount']?>" class="form-control" id="amount">
                    </div>
                    <div class="form-group">
                        <label for="paid">Paid</label>
                        <input type="number" min="0" name="paid"  value="<?php echo $purchase['paid']?>" class="form-control" id="paid">
                    </div>
                    <div class="form-group">
                        <label for="purchase_info">Purchase Info</label>
                        <input type="text" name="purchase_info" class="form-control" value="<?php echo $purchase['purchase_info']?>" id="purchase_info">
                    </div>
                    <?php if ($error != '') { ?>
                        <div class="mt-3 mb-1">
                            <span class="alert alert-danger d-block"><?php echo $error ?></span>
                        </div>
                    <?php } ?>
                    <div class="form-group mt-3">
                        <button type="submit" name="register" class="registerbtn btn btn-warning btn-block">Update
                            Purchase
                        </button>
                        <a href="list.php" class="btn btn-success  float-end">Back</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"></script>
</body>
</html>