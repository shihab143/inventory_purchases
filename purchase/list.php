<?php
session_start();
error_reporting(0);
include_once '../connection.php';

if(empty($_SESSION["login_user"])){  
    $_SESSION['success'] = "Please login first to start your session";
    header("location: ../index.php");
} 

$sql = "SELECT purchases.*,name from purchases,users where users.id = purchases.customer_id";
$result = mysqli_query($conn, $sql);
$row = mysqli_fetch_all($result, MYSQLI_ASSOC);

$name = $purchase_date_to = $purchase_date_from = $purchase_info = $amount_from = $amount_to = $paid_amount_from = $paid_amount_to = '';
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $name = $_POST['name'];
    $purchase_date_from = $_POST['purchase_date_from'];
    $purchase_date_to = $_POST['purchase_date_to'];
    $purchase_info = $_POST['purchase_info'];
    $amount_from = $_POST['amount_from'];
    $amount_to = $_POST['amount_to'];
    $paid_amount_from = $_POST['paid_amount_from'];
    $paid_amount_to = $_POST['paid_amount_to'];

    $condition = 'users.id = purchases.customer_id';
    if (!empty($name)) {
        $condition .= " AND name like '%$name%'";
    }
    if (!empty($purchase_info)) {
        $condition .= " AND purchase_info like '%$purchase_info%'";
    }
    if (!empty($purchase_date_from) && !empty($purchase_date_to)) {
        $condition .= " AND purchase_date between '$purchase_date_from' and '$purchase_date_to'";
    }
    if (!empty($amount_from) && !empty($amount_to)) {
        $condition .= " AND amount between $amount_from and $amount_to";
    }
    if (!empty($paid_amount_from) && !empty($paid_amount_to)) {
        $condition .= " AND paid between $paid_amount_from and $paid_amount_to";
    }
    $query = "SELECT purchases.*,name FROM purchases,users WHERE $condition";
    $query_execute = mysqli_query($conn, $query);
    $row = mysqli_fetch_all($query_execute, MYSQLI_ASSOC);
}
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Purchase List</title>
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://use.fontawesome.com/51f0941df6.js"></script>
</head>
<body>
<header class="p-3 bg-info text-white">
    <div class="container">
        <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
            <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
                <li><a href="../dashboard.php" class="nav-link px-2 text-white">Home</a></li>
                <li><a href="../customer/add.php" class="nav-link px-2 text-white">Customer Add</a></li>
                <li><a href="../customer/list.php" class="nav-link px-2 text-white">Customers List</a></li>
                <li><a href="add.php" class="nav-link px-2 text-white">Purchase Add</a></li>
                <li><a href="list.php" class="nav-link px-2 text-secondary">Purchases List</a></li>
            </ul>
            <div class="text-end">
                <a href="../logout.php" class="btn btn-outline-light me-2">Logout</a>
            </div>
        </div>
    </div>
</header>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <?php if (isset($_SESSION['success'])) { ?>
                <div class="mt-2 mb-2">
                    <span class="alert alert-success text-center d-block"><?php echo $_SESSION['success'] ?></span>
                </div>
                <?php unset($_SESSION['success']);
            } ?>
            <?php if (isset($_SESSION['error'])) { ?>
                <div class="mt-2 mb-2">
                    <span class="alert alert-danger text-center  d-block"><?php echo $_SESSION['error'] ?></span>
                </div>
                <?php unset($_SESSION['error']);
            } ?>
            <h3>Purchases List</h3>
            <div class="card">
                <div class="card-body">
                    <form action="list.php" method="post">
                        <div class="col-md-3 float-start">
                            <div class="form-group">
                                <label for="name">Customer Name</label>
                                <input type="text" name="name" value="<?php echo $name ?>" class="form-control"
                                       id="name">
                            </div>
                        </div>
                        <div class="col-md-3 float-start">
                            <div class="form-group">
                                <label for="purchase_date_from">Purchases Date From</label>
                                <input type="date" name="purchase_date_from" value="<?php echo $purchase_date_from ?>"
                                       class="form-control"
                                       id="purchase_date_from">
                            </div>
                        </div>
                        <div class="col-md-3 float-start">
                            <div class="form-group">
                                <label for="purchase_date_to">Purchases Date To</label>
                                <input type="date" name="purchase_date_to" value="<?php echo $purchase_date_to ?>"
                                       class="form-control"
                                       id="purchase_date_to">
                            </div>
                        </div>
                        <div class="col-md-3 float-start">
                            <div class="form-group">
                                <label for="purchases_info">Purchases Info</label>
                                <input type="text" name="purchase_info" value="<?php echo $purchase_info ?>"
                                       class="form-control"
                                       id="purchases_info">
                            </div>
                        </div>
                        <div class="col-md-3 float-start">
                            <div class="form-group" style="width: 50%;float: left;padding-right: 15px">
                                <label for="amount_from">Amount From</label>
                                <input type="number" name="amount_from" min="0" value="<?php echo $amount_from ?>"
                                       class="form-control" id="amount_from">
                            </div>
                            <div class="form-group" style="width: 40%;float: left;padding-left: 15px">
                                <label for="amount_to">Amount To</label>
                                <input type="number" name="amount_to" min="0" value="<?php echo $amount_to ?>"
                                       class="form-control" id="amount_to">
                            </div>
                        </div>
                        <div class="col-md-3 float-start">
                            <div class="form-group" style="width: 50%;float: left;padding-right: 15px">
                                <label for="paid_amount_from">Paid From</label>
                                <input type="number" name="paid_amount_from" min="0"
                                       value="<?php echo $paid_amount_from ?>"
                                       class="form-control" id="paid_amount_from">
                            </div>
                            <div class="form-group" style="width: 40%;float: left;padding-left: 15px">
                                <label for="paid_amount_to">Paid To</label>
                                <input type="number" name="paid_amount_to" min="0" value="<?php echo $paid_amount_to ?>"
                                       class="form-control" id="paid_amount_to">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="mt-3 col-md-3">
                            <div class="form-group float-start" style="padding-right: 10px">
                                <button type="submit" class="btn btn-warning btn-block">
                                    Search
                                </button>
                            </div>
                            <div class="form-group">
                                <a href="list.php" class="btn btn-success btn-block">
                                    Clear
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="table-responsive">
            <table class="table table-bordered mt-3">
                <thead>
                <tr>
                    <th>SI</th>
                    <th>Customer Name</th>
                    <th>Purchase Date</th>
                    <th>Purchase Info</th>
                    <th>Amount</th>
                    <th>Paid</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php for ($i = 0; $i < count($row); $i++) { ?>
                    <tr>
                        <td><?php echo $i + 1 ?></td>
                        <td><?php echo $row[$i]['name'] ?></td>
                        <td><?php echo $row[$i]['purchase_date'] ?></td>
                        <td><?php echo $row[$i]['purchase_info'] ?></td>
                        <td><?php echo $row[$i]['amount'] ?></td>
                        <td><?php echo $row[$i]['paid'] ?></td>
                        <td>
                            <a title="Edit Purchase" href="edit.php?id=<?php echo $row[$i]['id'] ?>"><i
                                        class=" fa fa-pencil-square-o"></i></a>
                            <a title="Delete Purchase" href="delete.php?id=<?php echo $row[$i]['id'] ?>"
                               class="text-danger"><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"></script>
<style>
    .col-md-3, .col-md-1 {
        padding-left: 15px;
        padding-right: 15px;
    }
    @media screen and (max-width: 600px){
        .wd-100{
            width:100% !important;
        }
        .float-start{float:none !important;}
    }
</style>
</body>
</html>