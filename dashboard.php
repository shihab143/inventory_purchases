<?php
session_start();
error_reporting(0);
include_once 'connection.php';

if(empty($_SESSION["login_user"])){  
    $_SESSION['success'] = "Please login first to start your session";
    header("location: index.php");
} 
$total_customer = mysqli_query($conn, 'SELECT count(id) as count_cus from users where type = 0');
$total_customer = mysqli_fetch_assoc($total_customer);

$total_purchases = mysqli_query($conn, 'SELECT sum(amount) as sum_amount, sum(paid) as sum_paid from purchases');
$total_purchases = mysqli_fetch_assoc($total_purchases);

$today_purchases = mysqli_query($conn, "SELECT sum(amount) as sum_amount from purchases where purchase_date ='" . date('Y-m-d') . "'");
$today_purchases = mysqli_fetch_assoc($today_purchases);
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Welcome to our inventory Management</title>
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

</head>
<body>
<header class="p-3 bg-info text-white">
    <div class="container">
        <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
            <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
                <li><a href="dashboard.php" class="nav-link px-2 text-secondary">Home</a></li>
                <li><a href="customer/add.php" class="nav-link px-2 text-white">Customer Add</a></li>
                <li><a href="customer/list.php" class="nav-link px-2 text-white">Customers List</a></li>
                <li><a href="purchase/add.php" class="nav-link px-2 text-white">Purchase Add</a></li>
                <li><a href="purchase/list.php" class="nav-link px-2 text-white">Purchases List</a></li>
            </ul>
            <div class="text-end">
                <a href="logout.php" class="btn btn-outline-light me-2">Logout</a>
            </div>
        </div>
    </div>
</header>
<div class="container">
    <div class="row">
        <div class="col-12 col-lg-12">
            <div class="alert alert-info text-center mt-5">
                Hi <?php echo $_SESSION['login_user']['name'] ?>, Welcome to your Dashboard
            </div>
        </div>
        <div class="col-lg-3 col-6">
            <div class="small-box bg-danger text-center">
                <div class="inner">
                    <h3><?php echo $total_customer['count_cus'] ?></h3>
                    <p>Total Customer</p>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-6">
            <div class="small-box bg-info text-center">
                <div class="inner">
                    <h3><?php echo empty($today_purchases['sum_amount']) ? 0 : $today_purchases['sum_amount']; ?>
                        Tk</h3>
                    <p>Today Purchases</p>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-6">
            <div class="small-box bg-success text-center">
                <div class="inner">
                    <h3><?php echo $total_purchases['sum_amount'] ?> Tk</h3>
                    <p>Total Purchases</p>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-6">
            <div class="small-box bg-warning text-center">
                <div class="inner">
                    <h3><?php echo $total_purchases['sum_paid'] ?> Tk</h3>
                    <p>Total Paid</p>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-6">
            <div class="small-box bg-success text-center">
                <div class="inner">
                    <h3><?php echo $total_purchases['sum_amount'] - $total_purchases['sum_paid'] ?> Tk</h3>
                    <p>Current Dues</p>
                </div>
            </div>
        </div>

    </div>
</div>
</div>
<style>
    .small-box {
        color: #fff !important;
    }

    .small-box {
        border-radius: 0.25rem;
        box-shadow: 0 0 1px rgb(0 0 0 / 13%), 0 1px 3px rgb(0 0 0 / 20%);
        display: block;
        margin-bottom: 20px;
        position: relative;
    }

    .small-box > .inner {
        padding: 10px;
    }

    .small-box .icon {
        color: rgba(0, 0, 0, .15);
        z-index: 0;
    }
</style>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"></script>
</body>
</html>